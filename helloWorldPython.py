import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BOARD)	#BOARD referencia os pinos fisicos
GPIO.setwarnings(False)		#Desativa mensagens do GPIO

GPIO.setup(7,GPIO.OUT)		#Define pino 7 como saida

print('LED ON')
GPIO.output(7,GPIO.HIGH)	#Ativa o pino 7 (com 1)

time.sleep(2)			#espera dois segundos

print('LED OFF')
GPIO.output(7,GPIO.LOW)		#Desativa o pino 7 (com 0)
