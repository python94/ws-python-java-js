package br.com.comunicacao.ws;

import java.io.Serializable;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/comunica")
public class ComunicacaoWS implements Serializable {

	private static final long serialVersionUID = -6090552836552327737L;
	
	private static final Set<Session> SESSIONS = ConcurrentHashMap.newKeySet();
	
	@PostConstruct
	public void init(){
		System.out.println("init");
	}

	@OnMessage
	public void onMessage(String message, Session session){
		System.out.println("ComunicacaoWS: " + message + "\tID = " + session.getId());
//			session.getBasicRemote().sendText("session.getBasicRemote().sendText");
//			Process process = Runtime.getRuntime().exec("py C:/Users/e-jgardin/Documents/ola.py");
//			Thread.sleep(5000);
//			process.destroy();
		    session.setMaxIdleTimeout((1000 * 60) * 5);
		for(Session s : SESSIONS){
			if(s.isOpen()){
				s.getAsyncRemote().sendText(message);
			}
		}
		
	}
	
	@OnOpen
	public void onOpen(Session session){
		SESSIONS.add(session);
	}
	
	@OnClose 
	public void onClose(Session session){
		SESSIONS.remove(session);
	}
	
}
