def lazy(up_to):
    index = 0
    while index < up_to:
        yield index
        index += 1
		
		
x = lazy(10)
while True:
	exit = input("")
	if exit == "exit":
		break
	print( next(x) )
	
